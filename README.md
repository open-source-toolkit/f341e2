# STM32温湿度数据采集与ONENET平台上传

## 项目简介

本项目提供了一个基于STM32微控制器的温湿度数据采集系统，并通过MQTT协议将采集到的数据上传至ONENET物联网平台。通过该资源文件，您可以学习如何使用STM32进行传感器数据采集，并通过MQTT协议实现与物联网平台的通信。

## 功能特点

- **温湿度数据采集**：使用STM32微控制器采集温湿度传感器的数据。
- **MQTT协议通信**：通过MQTT协议将采集到的数据上传至ONENET平台。
- **实时数据监控**：在ONENET平台上实时查看温湿度数据的变化。

## 使用说明

1. **硬件准备**：
   - STM32开发板（如STM32F103C8T6）
   - 温湿度传感器（如DHT11）
   - 网络模块（如ESP8266）

2. **软件准备**：
   - Keil uVision或其他STM32开发环境
   - ONENET平台账号

3. **配置与编译**：
   - 下载本仓库的源代码。
   - 根据您的硬件配置修改代码中的引脚定义和网络配置。
   - 编译并下载代码到STM32开发板。

4. **运行与测试**：
   - 启动STM32开发板，确保网络连接正常。
   - 在ONENET平台上查看上传的温湿度数据。

## 贡献与反馈

如果您在使用过程中遇到任何问题或有改进建议，欢迎提交Issue或Pull Request。我们非常欢迎社区的贡献，共同完善这个项目。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

---

希望这个项目能够帮助您快速上手STM32与物联网平台的开发，祝您学习愉快！